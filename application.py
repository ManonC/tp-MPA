#! /usr/bin/env python3
from db import DB

from flask import Flask, render_template
app = Flask(__name__)
app.debug = True

Gtodo = {'bob'   : ["se lever", "se doucher", "travailler"],
         'alice' : ["lire un livre", "manger", "se promener"],
         'toto'  : ["réviser", "boire de l'eau", "se doucher", "aller faire du sport"]}

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name = None):
    base = DB()
    donnees = base.get(name)
    listeElem = []
    for elem in donnees:
        listeElem.append(elem[1])
    return render_template("user.html", name = name, todo = listeElem)




@app.route('/users/')
def users():
    base = DB()
    data = []
    listeUsers = base.users()
    for user in listeUsers:
        data.append([user,base.get(user[0])])
    return render_template("users.html", todo = data)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
